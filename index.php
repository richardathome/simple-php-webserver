<?php
declare(strict_types=1);
/**
 * Simple PHP webserver example
 * Connects to a MySQL database and returns results as JSON.
 * This is not production ready code - it's a quick-start
 *
 * User: RichBuilds.com
 */

try {

    // what url are we responding to?
    $url = $_SERVER['REQUEST_URI'];

    switch ($url) {

        case '\products':

            $pdo = new PDO('mysql:dbname=database;host=127.0.0.1', 'username', 'password');
            $statement = $pdo->prepare("SELECT * FROM products");
            $statement->execute();
            $results = $statement->fetchAll(PDO::FETCH_ASSOC);

            header('All Products', true, 200);
            echo json_encode($results);
            break;

        default:
            header("Not found", true, 404);
            break;

    }

} catch (Exception $e) {
    echo 'Error at line ' . $e->getLine() . ' : ' . $e->getMessage();
}
